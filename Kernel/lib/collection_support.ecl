% ----------------------------------------------------------------------
% BEGIN LICENSE BLOCK
% Version: CMPL 1.1
%
% The contents of this file are subject to the Cisco-style Mozilla Public
% License Version 1.1 (the "License"); you may not use this file except
% in compliance with the License.  You may obtain a copy of the License
% at www.eclipseclp.org/license.
% 
% Software distributed under the License is distributed on an "AS IS"
% basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See
% the License for the specific language governing rights and limitations
% under the License. 
% 
% The Original Code is  The collection_support library for ECLiPSe.
% The Initial Developer of the Original Code is  Coninfer Ltd.
% Portions created by the Initial Developer are
% Copyright (C) 2017.  All Rights Reserved.
% 
% Contributor(s): Joachim Schimpf, Coninfer Ltd
% 
% END LICENSE BLOCK
% ----------------------------------------------------------------------

:- module(collection_support).

:- comment(categories, ["Data Structures","Constraints"]).
:- comment(summary, "Pure operations on lists and arrays").
:- comment(author, "Joachim Schimpf").
:- comment(copyright, "Joachim Schimpf, Coninfer Ltd").
:- comment(date, "$Date: 2017/08/01 13:34:01 $").

:- export
	eval_to_complete_list/2,
	eval_to_list/2,
	eval_to_array/2.

eval_to_complete_list(Expr, Zs) :-
	eval_to_list(Expr, Zs0),
	list_to_complete(Zs0, Zs, Zs0).

    list_to_complete(Xs, Xt) :-
	list_to_complete(Xs, Xt, Xs).

    list_to_complete(Xs, Xt, Xs0) :-
	sepia_kernel:list_end(Xs, Xs1),
	( var(Xs1), suspend(list_to_complete(Xs1, Xt, Xs0), 0, Xs1->inst)
	; Xs1==[], Xt=Xs0
	).


% Evaluate collections and their functions, return list.
eval_to_list(Xs, Zs) :- var(Xs), !,
	Zs=Xs.		% type check omitted
eval_to_list(eval(Expr), Zs) ?- !,
	( nonvar(Expr) -> eval_to_list(Expr, Zs)
	; suspend(eval_to_list(Expr, Zs), 0, Expr->inst)
	).
eval_to_list(Xe>>Ye, Zs) ?- !,
	eval_to_list(Xe, Xs),
	eval_to_list(Ye, Ys),
	pure_append(Xs, Ys, Zs).
eval_to_list(concat(Xe), Zs) ?- !,
	eval_to_list(Xe, Xs),
	pure_concat_any(Xs, Zs, []).
eval_to_list(subscript(A,I), Zs) ?- !,
	( var(A) ->
	    suspend(eval_to_list(subscript(A,I), Zs), 0, A->inst)
	; nonground(I,V) ->
	    suspend(eval_to_list(subscript(A,I), Zs), 0, V->inst)
	;
	    %%%TODO inst fault when A not instantiated deeply enough for I
	    subscript(A, I, X),
	    % coerce result if needed (Array element X may be a list)
	    coerce_to_list(X, Zs)
	).
% Expanded into three extra clauses below
%eval_to_list(Xs, Zs) :-
%	coerce_to_list(Xs, Zs).
eval_to_list([], Zs) :- !,
	Zs=[].
eval_to_list(Xs, Zs) :- Xs=[_|_], !,
	Zs=Xs.
eval_to_list(Xz, Zs) :- is_array(Xz),
	array_list(Xz, Zs).


% Coerce to list, no flattening
delay coerce_to_list(Xs,_Ys) if var(Xs).
coerce_to_list([], Ys) :- !,
	Ys=[].
coerce_to_list(Xs, Ys) :- Xs=[_|_], !,
	Ys=Xs.
coerce_to_list(Xz, Ys) :- is_array(Xz),
	array_list(Xz, Ys).


% Evaluate collections and their functions, return array.
eval_to_array(Xs, Zz) :- var(Xs), !,
	list_to_array(Xs, Zz, Xs).
eval_to_array(eval(Expr), Zz) :- !,
	( nonvar(Expr) -> eval_to_array(Expr, Zz)
	; suspend(eval_to_array(Expr, Zz), 0, Expr->inst)
	).
eval_to_array(Xe>>Ye, Zz) :- !,
	eval_to_array(Xe, Xz),
	eval_to_array(Ye, Yz),
	append_arrays(Xz, Yz, Zz).
eval_to_array(subscript(A,I), Zz) :- !,
	( var(A) ->
	    suspend(eval_to_array(subscript(A,I), Zz), 0, A->inst)
	; nonground(I,V) ->
	    suspend(eval_to_array(subscript(A,I), Zz), 0, V->inst)
	;
	    %%%TODO inst fault when A not instantiated deeply enough for I
	    subscript(A, I, X),
	    % coerce result if needed (Array element X may be a list)
	    coerce_to_array(X, Zz)
	).
eval_to_array(concat(Xe), Zz) :- !,
	eval_to_list(Xe, Xs),
	pure_concat_any(Xs, Zs, []),
	list_to_array(Zs, Zz, Zs).
% Expanded into two extra clauses below
%eval_to_array(Xs, Zz) :-
%	coerce_to_array(Xs, Zz).
eval_to_array(Xs, Yz) :- Xs=[_|Xs1], !,
	list_to_array(Xs1, Yz, Xs).
eval_to_array(Xz, Xz) :- is_array(Xz).


delay coerce_to_array(Xs,_Ys) if var(Xs).
coerce_to_array(Xs, Yz) :- Xs=[_|Xs1], !,
	list_to_array(Xs1, Yz, Xs).
coerce_to_array(Xz, Xz) :- is_array(Xz).


list_to_array(Xs, Xz, Xs0) :- var(Xz), !,
	sepia_kernel:list_end(Xs, Xs1),
	( var(Xs1), suspend(list_to_array(Xs1, Xz, Xs0), 0, Xz+Xs1->inst)
	; Xs1==[], array_list(Xz, Xs0)
	).
list_to_array(_Xs, Xz, Xs0) :-
	array_list(Xz, Xs0).


delay append_arrays(Xz, Yz, Zz) if var(Xz);var(Yz).
append_arrays(Xz, Yz, Zz) :- array_concat(Xz, Yz, Zz).


delay pure_append(Xs,_Ys,_Zs) if var(Xs).
pure_append([], Ys, Ys).
pure_append([X|Xs], Ys, [X|Zs]) :- pure_append(Xs, Ys, Zs).


% Append to list to list/array, giving list.
% Delays for incomplete lists
delay pure_append_any(Xs,_Ys,_Ys0) if var(Xs).
pure_append_any([], Ys, Ys0) :- !, Ys=Ys0.
pure_append_any([Xs|Xss], Ys, Ys0) :-
	Ys = [Xs|Ys1],
	pure_append(Xss, Ys0, Ys1).
pure_append_any(Xz, Ys, Ys0) :- is_array(Xz),
	( foreacharg(X,Xz), fromto(Ys,[X|Ys1],Ys1,Ys0) do true).

% Concatenate all elements (list/array) of a list, giving list.
delay pure_concat_any(Xs,_Ys,_Ys0) if var(Xs).
pure_concat_any([], Ys, Ys0) :- !, Ys=Ys0.
pure_concat_any([Xc|Xcs], Ys, Ys0) :-
	pure_append_any(Xc, Ys, Ys1),
	pure_concat_any(Xcs, Ys1, Ys0).


% ----------------------------------------------------------------------

:- comment(eval_to_list/2, [
    summary:"Equate a \"collection\" expression with a list",
    amode:(eval_to_list(?,?) is semidet),
    args:["Collection":"A term to be interpreted as a collection",
    	"Result":"The collection as list"],
    fail_if:"Collection is not a collection expression, or Result does not unify with the evaluation result",
    see_also:[eval_to_complete_list/2, eval_to_array/2,
    		collection_to_list/2, array_list/2, array_concat/3, append/3,
		is_list/1, is_array/1, subscript/3],
    desc:html("<P>\
   This is a constraint establising equality between a collection-valued
   expression and a list containing the same element as the collection.
   Evaluation delays if arguments are insufficiently instantiated.
</P><P>
   A \"collection\" can be either of the following data terms:
<DL>
   <DT><STRONG>List</STRONG><DD>
	The list is returned unchanged.
   <DT><STRONG>Array</STRONG><DD>
	The array is converted into a list, as with array_list/2.
</DL>
   In addition, the following \"collection expressions\" are allowed:
<DL>
   <DT><STRONG>Array[...]</STRONG><DD>
	Subscript-reference: Extract an element or sub-array from Array.
	If a single array element is extracted, this element must itself
	be a collection (array or list).
   <DT><STRONG>CollectionExpr1&gt;&gt;CollectionExpr2</STRONG><DD>
	The result is the concatenation of the two collections.
   <DT><STRONG>concat(CollectionExpr)</STRONG><DD>
	If the collection is nested (at least 2-dimensional), the top
	level of the structure is removed and the result is the
	concatenation of all its elements (which must be collections
	themselves).
   <DT><STRONG>eval(X)</STRONG><DD>
	If X is a list, array or collection expression, then eval(X)
	is equivalent to X alone.  If X is uninstantiated, then eval(X)
	indicates that X might be instantiated to any collection expression
	term.  Without the eval/1 wrapper, a free X is interpreted as an
	uninstantiated list.
</DL>
</P>
"),
    eg:"\
   ?- List=[a,b,c,d], eval_to_list(List, Result).
   Result = [a, b, c, d]

   ?- Arr=[](a,b,c,d), eval_to_list(Arr, Result).
   Result = [a, b, c, d]

   ?- Arr=[](a,b,c,d), eval_to_list(Arr[2..3], Result).
   Result = [b, c]


   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat, Result).
   Result = [[](a, b, c), [](d, e, f)]

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(concat(Mat), Result).
   Result = [a, b, c, d, e, f]

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat[1], Result).
   Result = [a, b, c]

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat[1,*], Result).
   Result = [a, b, c]

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat[*,2], Result).
   Result = [b, e]

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat[1..2,2], Result).
   Result = [b, e]

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat[1..2,2..3], Result).
   Result = [[](b, c), [](e, f)]

   ?- Mat=[]([](a,b,c),[](d,e,f)),
			eval_to_list(concat(Mat[1..2,2..3]), Result).
   Result = [b, c, e, f]


   ?- NL = [a,b,[c,d]], eval_to_list(NL, Result).
   Result = [a, b, [c, d]]

   ?- NL = [a,b,[](c,d)], eval_to_list(NL, Result).
   Result = [a, b, [](c, d)]

   ?- NA = [](a,b,[c,d]), eval_to_list(NA, Result).
   Result = [a, b, [c, d]]

   ?- NA = [](a,b,[c,d]), eval_to_list(NA[3], Result).
   Result = [c, d]


   ?- Xs=[a,b], Yz=[](c,d), eval_to_list(Xs>>Yz, Result).
   Result = [a, b, c, d]


   % Error cases where collections expected
   ?- eval_to_list(no_collection, Result).
   No (0.00s cpu)

   ?- eval_to_list(99, Result).
   No (0.00s cpu)

   ?- eval_to_list(concat([[1],2,[3]]), Result).
   No (0.00s cpu)


   % Cases with insufficient instantiation:

   ?- eval_to_list(Xs, R).
   R = Xs                % assumed to be a list (but no check)
   Yes

   ?- eval_to_list([1|Xs], R).
   R = [1|Xs]
   Yes

   ?- eval_to_list(Xs>>[3], R).
   R = R
   <delays until Xs instantiated>

   ?- eval_to_list([1]>>Ys, R).
   R = [1|Ys]                % assumed to be a list (but no check)
   Yes

   ?- eval_to_list([1]>>Ys, R).
   R = [1|Ys]                % assumed to be a list (but no check)
   Yes

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_list(Mat[I], R).
   R = R
   <delays until I instantiated>


   % Delayed instantiation of expression
   ?- eval_to_list(eval(X), [1,2,3]).
   X = X
   <delays until X instantiated>

   ?- eval_to_list(eval(X), Ys), X=[1]>>[2,3].
   Ys = [1, 2, 3]

   ?- eval_to_list(eval(X), [1,2,3]), X=[](1,2,3).
   Yes


   % Reverse direction
   ?- eval_to_list(Xs, [1,2,3]).
   Xs = [1,2,3]
   Yes
"]).

:- comment(eval_to_complete_list/2, [
    summary:"Equate a \"collection\" expression with a list",
    amode:(eval_to_complete_list(?,?) is semidet),
    args:["Collection":"A term to be interpreted as a collection",
    	"Result":"The collection as list"],
    fail_if:"Collection is not a collection expression, or Result does not unify with the evaluation result",
    see_also:[eval_to_list/2, eval_to_array/2,
    		collection_to_list/2, array_list/2, array_concat/3, append/3,
		is_list/1, is_array/1, subscript/3],
    desc:html("<P>\
   This is declaratively identical to eval_to_list/2.
</P><P>
   The only difference is that the Result argument will only be instantiated
   once the complete result lists (and its length) is known. As long
   as (subgoals of) eval_to_complete_list/2 delay, the Result variable
   is not touched.  This behaviour makes it easier to write predicates
   that wait for an input list: they can delay until Result gets instantiated,
   and then proceed, assuming that Result is a proper, terminated list.
</P>
"),
    eg:"\
   % Cases with insufficient instantiation:

   ?- eval_to_complete_list(Xs, R).
   R = R
   <delays until result fully known, R not bound>

   ?- eval_to_complete_list([1,2|Xs], R).
   R = R
   <delays until result fully known, R not bound>

   ?- eval_to_complete_list([1,2|Xs], R), Xs=[3].
   R = [1,2,3]
   Yes

   ?- eval_to_complete_list(Xz, R), Xz = [](1,2,3).
   R = [1,2,3]
   Yes

   ?- eval_to_complete_list([1]>>Ys, R).
   R = R
   <delays until result fully known, R not bound>

   ?- eval_to_complete_list([1]>>Ys, R), Ys = [2].
   R = [1,2]
   Yes

   % Other examples see eval_to_list/2
"]).

:- comment(eval_to_array/2, [
    summary:"Equate a \"collection\" expression with an array",
    amode:(eval_to_array(?,?) is semidet),
    args:["Collection":"A term to be interpreted as a collection",
    	"Result":"The collection as array"],
    fail_if:"Collection is not a collection expression, or Result does not unify with the evaluation result",
    see_also:[eval_to_complete_list/2, eval_to_array/2,
    		collection_to_list/2, array_list/2, array_concat/3, append/3,
		is_list/1, is_array/1, subscript/3],
    desc:html("<P>\
   This is a constraint establising equality between a collection-valued
   expression and an array containing the same element as the collection.
   Evaluation delays if arguments are insufficiently instantiated.
</P><P>
   A \"collection\" can be either of the following data terms:
<DL>
   <DT><STRONG>List</STRONG><DD>
	The list is converted into an array, as with array_list/2.
   <DT><STRONG>Array</STRONG><DD>
	The array is returned unchanged.
</DL>
   In addition, the following \"collection expressions\" are allowed:
<DL>
   <DT><STRONG>Array[...]</STRONG><DD>
	Subscript-reference: Extract an element or sub-array from Array.
	If a single array element is extracted, this element must itself
	be a collection (array or list).
   <DT><STRONG>CollectionExpr1&gt;&gt;CollectionExpr2</STRONG><DD>
	The result is the concatenation of the two collections.
   <DT><STRONG>concat(CollectionExpr)</STRONG><DD>
	If the collection is nested (at least 2-dimensional), the top
	level of the structure is removed and the result is the
	concatenation of all its elements (which must be collections
	themselves).
   <DT><STRONG>eval(X)</STRONG><DD>
	If X is a list, array or collection expression, then eval(X)
	is equivalent to X alone.  If X is uninstantiated, then eval(X)
	indicates that X might be instantiated to any collection expression
	term.  Without the eval/1 wrapper, a free X is interpreted as an
	uninstantiated list.
</DL>
</P>
"),
    eg:"\
   ?- List=[a,b,c,d], eval_to_array(List, Result).
   Result = [](a, b, c, d)

   ?- Arr=[](a,b,c,d), eval_to_array(Arr, Result).
   Result = [](a, b, c, d)

   ?- Arr=[](a,b,c,d), eval_to_array(Arr[2..3], Result).
   Result = [](b, c)


   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat, Result).
   Result = []([](a, b, c), [](d, e, f))

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(concat(Mat), Result).
   Result = [](a, b, c, d, e, f)

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat[1], Result).
   Result = [](a, b, c)

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat[1,*], Result).
   Result = [](a, b, c)

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat[*,2], Result).
   Result = [](b, e)

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat[1..2,2], Result).
   Result = [](b, e)

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat[1..2,2..3], Result).
   Result = []([](b, c), [](e, f))

   ?- Mat=[]([](a,b,c),[](d,e,f)),
			eval_to_array(concat(Mat[1..2,2..3]), Result).
   Result = [](b, c, e, f)


   ?- NL = [a,b,[c,d]], eval_to_array(NL, Result).
   Result = [](a, b, [c, d])

   ?- NL = [a,b,[](c,d)], eval_to_array(NL, Result).
   Result = [](a, b, [](c, d))

   ?- NA = [](a,b,[c,d]), eval_to_array(NA, Result).
   Result = [](a, b, [c, d])

   ?- NA = [](a,b,[c,d]), eval_to_array(NA[3], Result).
   Result = [](c, d)


   ?- Xs=[a,b], Yz=[](c,d), eval_to_array(Xs>>Yz, Result).
   Result = [](a, b, c, d)


   % Error cases where collections expected
   ?- eval_to_array(no_collection, Result).
   No (0.00s cpu)

   ?- eval_to_array(99, Result).
   No (0.00s cpu)

   ?- eval_to_array(concat([[1],2,[3]]), Result).
   No (0.00s cpu)


   % Cases with insufficient instantiation:

   ?- eval_to_array(Xs, R).
   R = R
   <delays until Xs (or R) instantiated>

   ?- eval_to_array([1|Xs], R).
   R = R
   <delays until Xs instantiated>

   ?- eval_to_array([1|Xs], R), Xs=[2].
   R = [](1,2)
   Yes

   ?- eval_to_array(Xs>>[3], R).
   R = R
   <delays until Xs instantiated>

   ?- eval_to_array(Xs>>[3], R), Xs=[1].
   R = [](1,2)
   Yes

   ?- eval_to_array([1]>>Ys, R).
   R = R
   <delays until Ys instantiated>

   ?- eval_to_array([1]>>Ys, R), Ys=[2].
   R = [](1,2)
   Yes

   ?- Mat=[]([](a,b,c),[](d,e,f)), eval_to_array(Mat[I], R).
   R = R
   <delays until I instantiated>


   % Delayed instantiation of expression
   ?- eval_to_array(eval(X), [](1,2,3)).
   X = X
   <delays until X instantiated>

   ?- eval_to_array(eval(X), Ys), X=[1]>>[2,3].
   Ys = [](1, 2, 3)

   ?- eval_to_array(eval(X), [](1,2,3)), X=[](1,2,3).
   Yes


   % Reverse direction
   ?- eval_to_array(Xs, [](1,2,3)).
   Xs = [1,2,3]
   Yes
"]).


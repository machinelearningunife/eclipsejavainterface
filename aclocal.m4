
# AC_ADD_CFLAG(flag_name)
# 
# This is like
# CFLAGS="$CFLAGS <flag_name>"
# but does it only if the compiler understands the command line flag.
# Should work for both C/CFLAGS and C++/CXXFLAGS

AC_DEFUN([AC_ADD_COMPILER_FLAG], [
    ac_saved_compiler_flags="$[]_AC_LANG_PREFIX[]FLAGS"
    _AC_LANG_PREFIX[]FLAGS="$[]_AC_LANG_PREFIX[]FLAGS -Werror $1"
    AC_COMPILE_IFELSE([
	AC_LANG_PROGRAM([], [return 0;])],
	[ _AC_LANG_PREFIX[]FLAGS="$ac_saved_compiler_flags $1"
	],
	[ _AC_LANG_PREFIX[]FLAGS="$ac_saved_compiler_flags"
	  AC_MSG_WARN([_AC_LANG_PREFIX compiler doesn't understand $1])
	])
])


# AC_ADD_IF_VALID_COMPILER_FLAG(var,flag_name)
#
# This is like
# var="$var <flag_name>"
# but does it only if the compiler understands the command line flag.
# Should work for both C and C++

AC_DEFUN([AC_ADD_IF_VALID_COMPILER_FLAG], [
    ac_saved_compiler_flags="$[]_AC_LANG_PREFIX[]FLAGS"
    _AC_LANG_PREFIX[]FLAGS="$[]_AC_LANG_PREFIX[]FLAGS -Werror $2"
    AC_COMPILE_IFELSE([
	AC_LANG_PROGRAM([], [return 0;])],
	[ $1="$[]$1 $2"
	],
	[ AC_MSG_WARN([_AC_LANG_PREFIX compiler doesn't understand $2])
	])

    _AC_LANG_PREFIX[]FLAGS="$ac_saved_compiler_flags"
])

